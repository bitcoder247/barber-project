import React from "react"
import AppProvider from "./src/context/app/AppProvider"

export const wrapRootElement = ({ element }) => {
  return <AppProvider>{element}</AppProvider>
}
