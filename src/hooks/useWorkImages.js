import { graphql, useStaticQuery } from "gatsby"

const useWorkImages = () => {
  const { data1, data2, data3 } = useStaticQuery(graphql`
    query {
      data1: allFile(
        filter: { relativeDirectory: { eq: "work/barber-photos/antarctica" } }
      ) {
        nodes {
          image: childImageSharp {
            fluid(maxWidth: 1600) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }

      data2: allFile(
        filter: { relativeDirectory: { eq: "work/barber-photos/grandcanyon" } }
      ) {
        nodes {
          image: childImageSharp {
            fluid(maxWidth: 1600) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
      data3: allFile(
        filter: { relativeDirectory: { eq: "work/barber-photos/flowers" } }
      ) {
        nodes {
          image: childImageSharp {
            fluid(maxWidth: 1600) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    }
  `)

  return [data1.nodes.slice(), data2.nodes.slice(), data3.nodes.slice()]
}

export default useWorkImages
