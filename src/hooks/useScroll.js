import { useRef } from "react"

const useScroll = () => {
  const htmlRef = useRef(null)
  const executeScroll = () => {
    // window.scrollTo({ behavior: "smooth", top: htmlRef.current.offsetTop })
    htmlRef.current.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "start",
    })
  }

  return [htmlRef, executeScroll]
}

export default useScroll
