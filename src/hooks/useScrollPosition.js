import { useLayoutEffect } from "react"

const isBrowser = typeof window !== "undefined"

const getScrollPosition = ({ element, useWindow }) => {
  if (!isBrowser) return { x: 0, y: 0 }

  const target = element ? element.current : document.body
  const position = target.getBoundingClientRect()

  return useWindow
    ? { x: window.scrollX, y: window.scrollY }
    : { x1: position.left, y1: position.top, y2: position.bottom }
}

// dependencies: [effect, deps, element, useWindow, wait]
const useScrollPosition = (effect, deps, currRef) => {
  useLayoutEffect(() => {
    const callBack = () => {
      const position = getScrollPosition({ useWindow: deps[deps.length - 2] })
      const currPos = getScrollPosition({
        element: currRef,
      })
      effect({ windowPos: position, currPos: currPos })
    }

    const handleScroll = () => {
      if (deps[deps.length - 1]) {
        setTimeout(callBack, deps[deps.length - 1]) // wait
      } else {
        callBack()
      }
    }

    window.addEventListener("scroll", handleScroll)

    return () => {
      window.removeEventListener("scroll", handleScroll)
    }
    // eslint-disable-next-line
  }, deps)
}

export default useScrollPosition
