import { SHOW_ELEMENT, MAKE_ACTIVE } from "../types"

const appReducer = (state, action) => {
  switch (action.type) {
    case SHOW_ELEMENT:
      console.log(action.item)
      return {
        ...state,
        [action.item]: !state[action.item],
      }
    case MAKE_ACTIVE:
      return {
        ...state,
        active: action.section,
        sections: state.sections.map(elem => {
          if (action.section === elem.section) {
            return {
              id: elem.id,
              section: elem.section,
              active: true,
            }
          } else {
            return {
              ...elem,
              active: false,
            }
          }
        }),
      }
    default:
      return state
  }
}

export default appReducer
