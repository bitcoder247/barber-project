import React, { useReducer } from "react"
import AppContext from "./AppContext"
import appReducer from "./appReducer"
import { SHOW_ELEMENT } from "../types"
import uuidv4 from "uuid/v4"

const AppProvider = ({ children }) => {
  const [state, dispatch] = useReducer(appReducer, {
    tabs: [
      {
        id: uuidv4(),
        section: "Home",
        active: false,
      },
      {
        id: uuidv4(),
        section: "About Me",
        active: false,
      },
      {
        id: uuidv4(),
        section: "My Work",
        active: false,
      },
      {
        id: uuidv4(),
        section: "Services",
        active: false,
      },
      {
        id: uuidv4(),
        section: "Contact Me",
        active: false,
      },
    ],
    showAbout: false,
    showService: false,
    showContact: false,
  })

  const handleShow = item => dispatch({ type: SHOW_ELEMENT, item: item })

  return (
    <AppContext.Provider
      value={{
        showAbout: state.showAbout,
        showService: state.showService,
        showContact: state.showContact,
        tabs: state.tabs,
        handleShow: handleShow,
        dispatch,
      }}
    >
      {children}
    </AppContext.Provider>
  )
}

export default AppProvider
