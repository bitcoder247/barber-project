import React, { useState } from "react"

import Layout from "../components/layout/layout"
import Showcase from "../components/layout/showcase"
import About from "../components/sections/about"
import Work from "../components/sections/work/work"
import Parallax from "../components/sections/parallax"
import Services from "../components/sections/services"
import Contact from "../components/sections/contact"
import useScroll from "../hooks/useScroll"
import StickyNavbar from "../components/layout/stickyNavbar"

const IndexPage = () => {
  const [showAbout, setShowAbout] = useState(false)
  const [showService, setShowService] = useState(false)
  const [showContact, setShowContact] = useState(false)

  const [homeRef, homeScroll] = useScroll()
  const [aboutRef, aboutScroll] = useScroll()
  const [workRef, workScroll] = useScroll()
  const [contactRef, contactScroll] = useScroll()
  const [serviceRef, serviceScroll] = useScroll()

  return (
    <Layout>
      <StickyNavbar
        showAbout={showAbout}
        setShowAbout={setShowAbout}
        setShowService={setShowService}
        setShowContact={setShowContact}
        aboutScroll={aboutScroll}
        workScroll={workScroll}
        serviceScroll={serviceScroll}
        contactScroll={contactScroll}
      />
      <Showcase
        ref={homeRef}
        aboutScroll={aboutScroll}
        workScroll={workScroll}
        serviceScroll={serviceScroll}
        contactScroll={contactScroll}
      />
      <About ref={aboutRef} showAbout={showAbout} />
      <Work ref={workRef} />
      <Parallax />
      <Services ref={serviceRef} showService={showService} />
      <Contact ref={contactRef} />
    </Layout>
  )
}

export default IndexPage
