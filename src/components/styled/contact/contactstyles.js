import styled from "@emotion/styled"
import mq from "../utils/breakpoints"

export const ContactsContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;

  ${mq("small", "min")} {
    justify-content: flex-start;
    flex-direction: column;
  }

  ${mq("desktop", "min")} {
    padding 1.2rem;
    flex-direction: row;
  }
`

export const ContactInfoContainer = styled("div")`
  position: relative;
  padding: 1.2rem;
  font-weight: 700;
  color: #eee;

  ${mq("small", "min")} {
    width: 100%;
  }

  ${mq("desktop", "min")} {
    width: 50%;
    height: 100%;
  }
`

export const ContactHeader = styled("h2")`
  font-weight: 700;
  color: #eee;
  ${mq("small", "min")} {
    font-size: 2.5rem;
    text-align: left;
  }

  ${mq("desktop", "min")} {
    text-align: left;
    font-size: 4rem;
  }
`

export const ContactDescription = styled("p")`
  font-size: 1.2rem;
  font-weight: 700;
  text-align: left;

  ${mq("small", "min")} {
    font-size: 1rem;
    text-align: left;
    margin-top: 0.5rem;
  }

  ${mq("desktop", "min")} {
    margin: 0.3rem 1rem 0 1rem;
    padding: 0 0.5rem;
    font-size: 1.2rem;
    text-align: left;
  }
`

export const ContactPersonal = styled("div")`
  display: flex;
  justify-content: flex-start;
  align-items: center;

  i {
    background-color: #eee;
    color: #000;
  }

  ${mq("small", "min")} {
    font-size: 1rem;
    padding: 1rem;
    i {
      margin-right: 0.5rem;
      padding: 0.5rem;
    }

    p {
      padding-left: 0.5rem;
      margin-left: 0.3rem;
      border-left: 2px solid #eee;
    }
  }

  ${mq("desktop", "min")} {
    font-size: 1rem;
    padding: 1rem;
    margin: 0.5rem;

    i {
      margin-right: 0.5rem;
      padding: 0.5rem;
    }

    p {
      padding-left: 0.5rem;
      margin-left: 0.3rem;
      border-left: 2px solid #eee;
    }
  }
`

export const ContactForm = styled("div")`
  padding: 1.2rem;

  label {
    display: block;
    font-size: 1rem;
    color: #eee;
    font-weight: 700;
    margin-bottom: 0.5rem;
  }

  ${mq("small", "min")} {
    width: 100%;

    input {
      font-size: 1rem;
      padding: 0.3rem 0;
      width: 100%;
    }
  }
  ${mq("desktop", "min")} {
    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 50%;
    height: 100%;

    input {
      font-size: 1rem;
      padding: 0.3rem 0;
      width: 80%;
    }
  }
`

export const VerticalLine = styled("div")`
  position: absolute;
  margin: auto 0;
  top: 0;
  right: 0;
  bottom: 0;
  background-color: #eee;

  width: 3px;
  height: 60%;

  ${mq("small", "min")} {
    display: none;
  }

  ${mq("desktop", "min")} {
    display: block;
  }
`

export const ContactButton = styled("button")`
  position: relative;
  border: 1px solid #eee;
  padding: 0.3rem 1.2rem;
  margin-bottom: 2rem;
  font-size: 1rem;
  color: #eee;
  overflow: hidden;
  transition: all 400ms ease-in;
  &::before {
    content: "";
    position: absolute;
    background-color: #ffc300;
    top: 0;
    left: -100px;
    right: -100px;
    bottom: 0;
    width: 250px;
    height: 250px;
    margin: auto;
    border-radius: 50%;
    overflow: hidden;
    transform: scale(0);
    z-index: -1;
    transition: all 400ms ease-in;
  }
  &:hover::before {
    transform: scale(1);
  }

  &:hover {
    color: #000;
    border-color: #ffc300;
    transform: translateY(-5px);
  }
`
