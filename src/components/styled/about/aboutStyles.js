import styled from "@emotion/styled"
import { keyframes } from "@emotion/core"
import Img from "gatsby-image"
import svgCrown from "../../../../content/svgs/royalty-crown.svg"
import mq from "../utils/breakpoints"

export const AboutSection = styled("section")`
  display: flex;

  position: relative;
  width: 100%;
  background-color: #eee;
  overflow: hidden;
  z-index: 0;
  opacity: 0;

  &.about-appear,
  &.about-enter {
    opacity: 0;
  }

  &.about-appear-active,
  &.about-enter-active {
    opacity: 1;
    transition: opacity 600ms ease-in-out;
  }

  &.about-appear-done,
  &.about-enter-done {
    opacity: 1;
  }

  ${mq("small", "min")} {
    flex-direction: column-reverse;
  }

  ${mq("desktop", "min")} {
    height: 100vh;
    flex-direction: row;
  }
`

export const ImageContainer = styled("div")`
  display: flex;
  position: relative;
  justify-content: center;
  align-items: center;
  overflow-y: visible;

  ${mq("small", "min")} {
    width: 100%;
    margin-top: 0.5rem;
  }

  ${mq("desktop", "min")} {
    width: 50%;
    margin: 0;
  }
`

export const ImageWrapper = styled("div")`
  ${mq("small", "min")} {
    width: 100%;
    height: 350px;
  }
  ${mq("phoneSide", "min")} {
    width: 350px;
    height: 350px;
  }

  ${mq("tablet", "min")} {
    border: 10px solid #000;
    height: 500px;
    width: 500px;
  }
`

export const AboutTextContainer = styled("div")`
  position: relative;
  z-index: 0;

  ${mq("small", "min")} {
    width: 100%;
  }

  ${mq("desktop", "min")} {
    width: 50%;
  }
`

export const AboutDivider = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;
  padding: 0.2rem 0;
  margin: 0.2rem 0;
`

export const HorizontalLine = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #000;
  width: 100px;
  height: 2px;
  transform: scaleX(0);
  transform-origin: 50% 0%;

  &.line-appear,
  &.line-enter {
    transform: scaleX(0);
  }

  &.line-appear-active,
  &.line-enter-active {
    transform: scaleX(1);
    transition: all 1200ms ease-in;
  }

  &.line-appear-done,
  &.line-enter-done {
    transform: scaleX(1);
  }
`

const spin = keyframes`
  from {
    transform: rotate(45deg);
  }

  to {
    transform: rotate(405deg);
  }
`

export const Box = styled("div")`
  width: 15px;
  height: 15px;
  margin: 0.5rem;
  border: 2px solid #000;

  animation: ${spin} 2s infinite;
`

export const SvgCrown = styled(svgCrown)`
  width: 25px;
  height: 25px;
  margin: 0 0.3rem 0.3rem 0.3rem;
`

export const Horizontal = styled("div")`
  width: 50px;
  height: 3px;
  background-color: #000;
  margin-bottom: 0.3rem;
  opacity: 0;
  transform: scaleX(0);

  &.bar-appear,
  &.bar-enter {
    opacity: 0;
    transform: scaleX(0);
  }

  &.bar-appear-active,
  &.bar-enter-active {
    opacity: 1;
    transform: scaleX(1);
    transition: all 1200ms ease-in-out;
  }

  &.bar-appear-done,
  &.bar-enter-done {
    opacity: 1;
    transform: scaleX(1);
  }
`

export const Circle = styled("div")`
  width: 7px;
  height: 7px;
  background-color: #000;
  margin: 0.3rem;
  border-radius: 50%;
`

export const AboutHeader = styled("h2")`
  font-weight: 700;
  text-align: center;
  font-size: 1.75rem;
  padding: 0.5rem 0 0 0;
`

export const AboutDescriptors = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;

  h3 {
    padding: 0.3rem 0;
    font-size: 1.1rem;
  }

  p {
    padding: 0.2rem 0.5rem;
    font-size: 1rem;
    font-weight: 700;
    text-align: center;
  }

  ${mq("small", "min")} {
    flex-direction: column;
  }
  ${mq("phoneSide", "min")} {
    font-size: 1.5rem;
    text-align: center;
    padding: 0.5rem 0.2rem;
  }
`

export const AboutImage = styled(Img)`
  ${mq("small", "min")} {
    width: 100%;
  }
`

export const AboutLinks = styled("a")`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 0.5rem 0 0.5rem;
  width: 35px;
  height: 35px;
  font-size: 1.5rem;
  color: #fff;
  background-color: #6c3483;
  border-radius: 50%;
  transition: background-color 400ms ease-in;
  &:hover {
    background-color: #9b59b6;
  }
`

const beating = keyframes`
  0% {
    transform: rotate(0deg) scale(0.8);
  }
  5% {
    transform: rotate(0deg) scale(0.9);
  }
  10% {
    transform: rotate(0deg) scale(0.8);
  }
  15% {
    transform: rotate(0deg) scale(1);
  }
  50% {
    transform: rotate(0deg) scale(0.8);
  }
  100% {
    transform: rotate(0deg) scale(0.8);
  }
`

export const HeartStyles = styled("span")`
  display: inline-block;
  padding: 0 0.2rem;
  margin-left: 0.1rem;
  color: #c70039;
  transform: rotate(45deg);

  animation: ${beating} 2s infinite;
`

export const BackgroundYellow = styled("div")`
  position: absolute;
  top: 0;
  background-color: rgba(255, 195, 0, 0.3);

  &.box-1 {
    left: 47%;
    width: 150px;
    height: 150px;
    transform: rotate(60deg) translate(-40%, 30%);
    z-index: -2;
  }

  &.box-2 {
    width: 300px;
    height: 300px;
    left: 27%;
    transform: rotate(30deg) translate(-10%, 80%);
    z-index: -1;
  }

  ${mq("small", "min")} {
    display: none;
  }

  ${mq("desktop", "min")} {
    display: block;
  }
`

export const BackgroundPink = styled("div")`
  position: absolute;
  top: 0;
  background-color: rgba(242, 215, 213, 0.8);

  &.box-1 {
    left: 43%;
    width: 200px;
    height: 200px;
    z-index: -1;
    transform: rotate(175deg) translate(60%, -30%);
  }
  &.box-2 {
    top: unset;
    bottom: -20%;
    left: 30%;
    width: 700px;
    height: 700px;
    z-index: -1;
    transform: rotate(68deg) translate(40%, 30%);
  }

  ${mq("small", "min")} {
    display: none;
  }

  ${mq("desktop", "min")} {
    display: block;
  }
`

export const VerticalDivider = styled("div")`
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  margin: auto 0;
  height: 70%;
  width: 4px;
  background-color: #000;
  border-radius: 10px;

  ${mq("small", "min")} {
    display: none;
  }

  ${mq("desktop", "min")} {
    display: block;
  }
`
