const breakpoints = {
  small: "300",
  phone: "375",
  phoneSide: "550",
  phoneLg: "650",
  tablet: "768",
  tabletLg: "992",
  desktop: "1200",
}

const mq = (n, str) => {
  const bpArray = Object.keys(breakpoints).map(key => [key, breakpoints[key]])

  const [result] = bpArray.reduce((acc, [name, size]) => {
    if (n === name) {
      return [...acc, `@media screen and (${str}-width: ${size}px)`]
    }
    return acc
  }, [])

  return result
}

export default mq

// const breakpoints = [375, 768, 992, 1200]

// const mq = breakpoints.map(bp => `@media screen and (min-width: ${bp}px)`)

// export default mq
