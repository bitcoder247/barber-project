import styled from "@emotion/styled"

const Container = styled("div")`
  max-width: 1200px;
  margin: 0 auto;
  width: 100%;
  height: 100%;
`

export default Container
