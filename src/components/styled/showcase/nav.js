import styled from "@emotion/styled"
import mq from "../utils/breakpoints"

const Nav = styled("nav")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 1rem;
  opacity: 0;

  h1 {
    letter-spacing: 0.3rem;
    color: #fff;
  }

  ul {
    display: flex;
    align-items: center;
    li {
      padding: 0 0.75rem;
    }
  }

  &.nav-appear,
  &.nav-enter {
    opacity: 0;
  }

  &.nav-appear-active,
  &.nav-enter-active {
    opacity: 1;
    transition: opacity 800ms ease-in;
  }

  &.nav-appear-done,
  &.nav-enter-done {
    opacity: 1;
  }

  ${mq("small", "min")} {
    display: block;
    padding: 1rem 0;
    h1 {
      text-align: center;
      margin-bottom: 1.5rem;
    }
    ul {
      justify-content: space-around;
    }
  }
  ${mq("tablet", "min")} {
    display: flex;
    h1 {
      text-align: left;
      margin-bottom: 0;
    }
    ul {
      justify-content: normal;
    }
  }
`

export default Nav
