import styled from "@emotion/styled"
import mq from "../utils/breakpoints"

const NavButton = styled("button")`
  position: relative;
  padding: 0 0 0.3rem 0;
  font-size: 1rem;
  color: #fff;

  &::after {
    content: "";
    display: block;
    position: absolute;
    left: 0;
    bottom: 0;
    width: 100%;
    border-bottom: solid #ffc300 2px;
    transform: scaleX(0);
    transition: transform 300ms ease-in-out;
  }

  &:hover::after {
    transform: scaleX(1);
  }

  ${mq("small", "min")} {
    font-size: 0.7rem;
  }
  ${mq("phone", "min")} {
    font-size: 0.8rem;
  }
`

export default NavButton
