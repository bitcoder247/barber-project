import styled from "@emotion/styled"
import mq from "../utils/breakpoints"
import IconBadge from "../../../../content/svgs/hair-salon-badge.svg"

export const ShowcaseHeader = styled("div")`
  display: flex;
  align-items: center;
  flex-direction: column;
  flex-grow: 3;
  opacity: 0;
  padding: 0.50rem 0;

  &.showcase-appear,
  &.showcase-enter {
    opacity: 0;
  }

  &.showcase-appear-active,
  &.showcase-enter-active {
    opacity: 1;
    transition: opacity: 500ms ease-in;
  }

  &.showcase-appear-done,
  &.showcase-enter-done {
    opacity: 1;
  }
`

export const HeaderContainer = styled("div")`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const BorderOne = styled("div")`
  width: 100%;
  height: 3px;
  background-color: #fff;
  transform: scaleX(0);
  transform-origin: 0% 50%;

  &.border-appear,
  &.border-enter {
    transform: scaleX(0);
  }
  &.border-appear-active,
  &.border-enter-active {
    transform: scaleX(1);
    transition: transform 1100ms ease-in;
  }

  &.border-appear-done,
  &.border-enter-done {
    transform: scaleX(1);
  }
`

export const BorderTwo = styled("div")`
  width: 100%;
  height: 3px;
  background-color: #fff;
  transform: scaleX(0);
  transform-origin: 100% 0%;

  &.border-appear,
  &.border-enter {
    transform: scaleX(0);
  }
  &.border-appear-active,
  &.border-enter-active {
    transform: scaleX(1);
    transition: transform 1100ms ease-in;
  }

  &.border-appear-done,
  &.border-enter-done {
    transform: scaleX(1);
  }
`

export const HeadingOne = styled("h1")`
  padding: 0 0.3rem;
  font-weight: normal;
  color: #fff;

  ${mq("small", "min")} {
    font-size: 1.3rem;
  }
  ${mq("phone", "min")} {
    font-size: 1.5rem;
  }
  ${mq("phoneSide", "min")} {
    font-size: 1.5rem;
  }
  ${mq("phoneLg", "min")} {
    font-size: 2rem;
  }
  ${mq("tablet", "min")} {
    font-size: 2.5rem;
  }
`

export const HeadingTwo = styled("h1")`
  font-family: "Cookie", cursive;
  font-weight: normal;
  color: #fff;
  padding: 0 0.75rem;
  align-self: flex-end;

  ${mq("small", "min")} {
    font-size: 2.3rem;
  }
  ${mq("phone", "min")} {
    font-size: 2.5rem;
  }
  ${mq("phoneSide", "min")} {
    font-size: 2.7rem;
  }
  ${mq("phoneLg", "min")} {
    font-size: 3.2rem;
  }
  ${mq("tablet", "min")} {
    font-size: 3.5rem;
  }
`

export const HeadingThree = styled("h1")`
  font-size: 1.5rem;
  font-weight: 700;
  color: #fff;

  ${mq("small", "min")} {
    font-size: 2.3rem;
  }
  ${mq("phone", "min")} {
    font-size: 2.5rem;
  }
  ${mq("phoneSide", "min")} {
    font-size: 2.7rem;
  }
  ${mq("phoneLg", "min")} {
    font-size: 3.2rem;
  }
  ${mq("tablet", "min")} {
    font-size: 3.5rem;
  }
`

export const HeadingFour = styled("h1")`
  font-size: 1.2rem;
  font-weight: 700;
  color: #fff;

  ${mq("small", "min")} {
    font-size: 2.1rem;
  }
  ${mq("phone", "min")} {
    font-size: 2.5rem;
  }
  ${mq("phoneSide", "min")} {
    font-size: 2.7rem;
  }
  ${mq("phoneLg", "min")} {
    font-size: 3.2rem;
  }
  ${mq("tablet", "min")} {
    font-size: 3.5rem;
  }
`

export const HorizontalContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 60%;
  overflow: hidden;
`

export const BoxContainer = styled("div")`
  display: flex;
  flex-direction: column;
  padding: 0 0.5rem;
  transform: rotate(0deg);

  &.box-appear,
  &.box-enter {
    transform: rotate(0deg);
  }

  &.box-appear-active,
  &.box-enter-active {
    transform: rotate(-45deg);
    transition: transform 1200ms ease-in;
  }

  &.box-appear-done,
  &.box-enter-done {
    transform: rotate(-45deg);
  }
`

export const Box1 = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 10px;
  height: 10px;
  border-right: 1px solid #fff;
  border-bottom: 1px solid #fff;
`

export const Box2 = styled("div")`
  display: inline-block;
  width: 10px;
  height: 10px;
  border-left: 1px solid #fff;
  border-bottom: 1px solid #fff;
`

export const Box3 = styled("div")`
  width: 10px;
  height: 10px;
  border-top: 1px solid #fff;
  border-right: 1px solid #fff;
`

export const Box4 = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 10px;
  height: 10px;
  border-left: 1px solid #fff;
  border-top: 1px solid #fff;
`

export const Circle = styled("span")`
  width: 3px;
  height: 3px;
  border-radius: 50%;
  background-color: #fff;
`

export const RibbonContainer = styled("div")`
  width: 100%;
  position: relative;
  margin: 5% auto;
  overflow-x: visible;
  background-color: #fe4343;

  &::after,
  &::before {
    content: "";
    position: absolute;
    bottom: -0.6rem;
    display: block;
    border: 1.11rem solid #e83f3f;
    z-index: -1;
  }
  &::before {
    left: -1.2rem;
    border-right-width: 0.7rem;
    border-left-color: transparent;
  }
  &::after {
    right: -1.2rem;
    border-right-width: 0.7rem;
    border-right-color: transparent;
  }
`

export const RibbonHeadingOne = styled("h3")`
  display: inline-block;
  width: 50%;
  height: 100%;
  font-family: "Cookie";
  text-align: center;
  color: #fff;
  &::before {
    content: "";
    position: absolute;
    display: block;
    border-style: solid;
    border-color: #ca3737 transparent;
    bottom: -0.58rem;
  }

  &::before {
    left: 0;
    border-width: 0.6rem 0 0 0.6rem;
  }
`
export const RibbonHeadingTwo = styled("h3")`
  display: inline-block;
  width: 50%;
  height: 100%;
  font-family: "Cookie";
  text-align: center;
  color: #fff;
  &::before {
    content: "";
    position: absolute;
    display: block;
    border-style: solid;
    border-color: #ca3737 transparent;
    bottom: -0.58rem;
  }

  &::before {
    right: 0;
    border-width: 0.6rem 0.6rem 0 0;
  }
`
export const RibbonEmblem = styled(IconBadge)`
  position: absolute;
  width: 40px;
  height: 40px;
  left: 50%;
  right: 50%;
  transform: translate(-50%, -10%);
  fill: #000;
  background-color: #ffc300;
  border-radius: 50%;
`
