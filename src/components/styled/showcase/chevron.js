import styled from "@emotion/styled"

export const ChevronContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 0.5rem;
`

export const ChevronButton = styled("button")`
  font-size: 1rem;
  color: rgba(255, 255, 255, 0.6);
  transition: color 200ms ease-in;
  &:hover {
    color: #fff;
  }
`
