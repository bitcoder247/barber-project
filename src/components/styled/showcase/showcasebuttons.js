import styled from "@emotion/styled"
import mq from "../utils/breakpoints"

export const ShowcaseButtonContainer = styled("div")`
  display: flex;
  justify-content: space-around;
  align-items: center;
  padding: 2rem 0;
  margin-top: 1rem;
  flex-grow: 2;
  ${mq("small", "min")} {
    flex-direction: column;
  }
  ${mq("phoneSide", "min")} {
    flex-direction: row;
  }
`

export const ShowcaseButton = styled("button")`
  position: relative;
  border: 1px solid #eee;
  padding: 0.3rem 1.2rem;
  margin-bottom: 2rem;
  font-size: 1rem;
  color: #eee;
  overflow: hidden;
  transition: all 400ms ease-in;
  &::before {
    content: "";
    position: absolute;
    background-color: #ffc300;
    top: 0;
    left: -100px;
    right: -100px;
    bottom: 0;
    width: 250px;
    height: 250px;
    margin: auto;
    border-radius: 50%;
    overflow: hidden;
    transform: scale(0);
    z-index: -1;
    transition: all 400ms ease-in;
  }
  &:hover::before {
    transform: scale(1);
  }

  &:hover {
    color: #000;
    border-color: #ffc300;
    transform: translateY(-5px);
  }
`
