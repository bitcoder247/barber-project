import styled from "@emotion/styled"
import Haircut from "../../../../content/svg/haircut.svg"
import mq from "../utils/breakpoints"

export const IconContainer = styled("div")`
  overflow-y: hidden;
  padding: 0 1.75rem;
  ${mq("phoneSide", "max")} {
    margin: 0;
    padding: 1rem;
  }
`

export const IconRing = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 80px;
  height: 80px;
  border: solid #fff 2px;
  border-radius: 50%;
  opacity: 0;
  &.ring-appear {
    opacity: 0;
  }
  &.ring-appear-active {
    opacity: 1;
    transition: all 800ms ease-in;
  }
  &.ring-appear-done {
    opacity: 1;
  }
  ${mq("phoneSide", "max")} {
    width: 60px;
    height: 60px;
  }
`

export const Hairicon = styled(Haircut)`
  width: 60%;
  height: 60%;
  opacity: 0;
  transform: scale(0);
  fill: #fff;

  &.icon-appear {
    opacity: 0;
    transform: scale(0);
  }
  &.icon-appear-active {
    opacity: 1;
    transform: scale(1);
    transition: all 1500ms ease-in-out;
  }
  &.icon-appear-done {
    opacity: 1;
    transform: scale(1);
  }
`
