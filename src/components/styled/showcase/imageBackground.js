import BackgroundImage from "gatsby-background-image"
import styled from "@emotion/styled"

export const ShowcaseContainer = styled("div")`
  width: 100%;
  height: 100%;
`

export const ImageBackground = styled(BackgroundImage)`
  position: relative;
  width: 100%;
  height: 100%;
  padding: 0.75rem;
`

// export const Image = styled(Img)`
//   width: 100%;
//   height: 100%;
// `

// export const Image = styled(Img)`
//   ${mq("desktop", "min")} {
//     width: 55%;
//     height: 100%;
//   }

//   @media screen and (max-width: 1199px) {
//     width: 55%;
//     height: 100%;
//   }

//   ${mq("tabletLg", "max")} {
//     width: 100%;
//     height: 100%;
//   }
// `

export const Darkenbackground = styled("div")`
  position: absolute;
  background-color: rgba(0, 0, 0, 0.7);
  top: 0;
  left: 0;
  z-index: -1;
  width: 100%;
  height: 100%;
`

// export const Coloredbackground = styled("div")`
//   position: absolute;
//   background-color: #3e382f;
//   width: 45%;
//   height: 100%;
//   top: 0;
//   right: 0;
//   z-index: -1;
//   ${mq("tabletLg", "max")} {
//     display: none;
//   }
// `
