import styled from "@emotion/styled"
import mq from "../utils/breakpoints"
import badge from "../../../../content/svgs/hair-salon-badge.svg"
import maleHair from "../../../../content/svgs/male-hair-of-head.svg"
import shaver from "../../../../content/svgs/shaver.svg"
import washing from "../../../../content/svgs/hairdresser-washing.svg"
import haircut from "../../../../content/svgs/hairdresser-with-scissors-cutting-the-hair.svg"
import razor from "../../../../content/svgs/razor-hair-salon-tool.svg"

export const ServicesContainer = styled("div")`
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  padding: 0.5rem 0;

  ${mq("small", "min")} {
    width: 100%;
  }
`

export const MainHeading = styled("h1")`
  font-family: "Cookie";
  font-weight: 400;
  padding: 0.5rem;
  text-align: center;
  background-color: #000;
  color: #eee;
  ${mq("small", "min")} {
    grid-column-start: 2;
    grid-column-end: 12;
    font-size: 2rem;

  ${mq("tablet", "min")} {
    font-size: 2.5rem;
  }

  ${mq("desktop", "min")} {
    font-size: 3rem;
  }

  
`

export const Divider = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  padding: 0.3rem 0;
`

export const HorizontalLine = styled("div")`
  width: 60px;
  height: 2px;
  background-color: #000;
`

export const Circle = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  width: 47px;
  height: 47px;
  background-color: #000;
  padding: 0.1rem;
  margin: 0.3rem;
  overflow: hidden;
`

export const BoxContainer = styled("div")`
  display: flex;
  flex-direction: column;
  padding: 0 0.5rem;
  transform: rotate(45deg);
`

export const Box1 = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 10px;
  height: 10px;
  border-right: 1px solid #000;
  border-bottom: 1px solid #000;
`

export const Box2 = styled("div")`
  display: inline-block;
  width: 10px;
  height: 10px;
  border-left: 1px solid #000;
  border-bottom: 1px solid #000;
`

export const Box3 = styled("div")`
  width: 10px;
  height: 10px;
  border-top: 1px solid #000;
  border-right: 1px solid #000;
`

export const Box4 = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 10px;
  height: 10px;
  border-left: 1px solid #000;
  border-top: 1px solid #000;
`

export const SecondaryHeaderContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 1rem;
  margin: 1rem;
  background-color: #ffc300;
  border-top: 4px solid #000;
  border-bottom: 4px solid #000;

  ${mq("small", "min")} {
    &.header-one {
      grid-column-start: 2;
      grid-column-end: 12;
    }

    &.header-two {
      grid-column-start: 2;
      grid-column-end: 12;
    }
    &.header-three {
      grid-column-start: 2;
      grid-column-end: 12;
    }
    &.header-four {
      grid-column-start: 2;
      grid-column-end: 12;
    }
    &.header-five {
      grid-column-start: 2;
      grid-column-end: 12;
    }
    &.header-six {
      grid-column-start: 2;
      grid-column-end: 12;
    }
  }

  ${mq("tabletLg", "min")} {
    &.header-one {
      grid-column-start: 3;
      grid-column-end: 11;
    }

    &.header-two {
      grid-column-start: 3;
      grid-column-end: 11;
    }
    &.header-three {
      grid-column-start: 3;
      grid-column-end: 11;
    }
    &.header-four {
      grid-column-start: 3;
      grid-column-end: 11;
    }
    &.header-five {
      grid-column-start: 3;
      grid-column-end: 11;
    }
    &.header-six {
      grid-column-start: 3;
      grid-column-end: 11;
    }
  }
  ${mq("desktop", "min")} {
    &.header-one {
      grid-column-start: 2;
      grid-column-end: 6;
    }

    &.header-two {
      grid-column-start: 8;
      grid-column-end: 12;
    }
    &.header-three {
      grid-column-start: 2;
      grid-column-end: 6;
    }
    &.header-four {
      grid-column-start: 8;
      grid-column-end: 12;
    }
    &.header-five {
      grid-column-start: 2;
      grid-column-end: 6;
    }
    &.header-six {
      grid-column-start: 8;
      grid-column-end: 12;
    }
  }
`

export const SecondaryHeader = styled("h2")`
  font-weight: 700;
  text-align: center;
  color: #000;
  padding: 0.2rem;

  ${mq("small", "min")} {
    font-size: 1.2rem;
  }
  ${mq("tablet", "min")} {
    font-size: 1.5rem;
  }
`

export const Description = styled("h3")`
  font-weight: 700;
  padding: 0.5rem;
  margin: 0.3rem 0;
  text-align: center;

  ${mq("small", "min")} {
    font-size: 0.8rem;
  }
  ${mq("tablet", "min")} {
    font-size: 1rem;
  }
`

export const SecondarySquare = styled("div")`
  width: 5px;
  height: 5px;
  padding: 0.2rem;
  margin: 0 1rem;
  overflow: hidden;
  background-color: #000;
`

export const SecondarySvgContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 40px;
  height: 50px;
  margin: 0.3rem;
  padding: 0.1rem;
`

export const SecondaryHorizontal = styled("div")`
  width: 25px;
  height: 2px;
  background-color: #000;
`

export const BarberBadge = styled(badge)`
  width: 46px;
  height: 46px;
  fill #fff;
`

export const HairCutSvg = styled(haircut)`
  width: 30px;
  height: 35px;
  fill #000;
`
export const RazorSvg = styled(razor)`
  width: 30px;
  height: 30px;
  fill #000;
`
export const ShapeUpSvg = styled(shaver)`
  width: 30px;
  height: 30px;
  fill #000;
  transform: rotate(180deg);
`
export const MassageSvg = styled(washing)`
  width: 30px;
  height: 30px;
  fill #000;
`
export const StyleHairSvg = styled(maleHair)`
  width: 35px;
  height: 35px;
  fill #000;
`
