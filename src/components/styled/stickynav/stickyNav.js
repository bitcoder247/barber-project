import styled from "@emotion/styled"
import mq from "../utils/breakpoints"

export const Navbar = styled("nav")`
  &.stickyNav {
    display: flex;
  }

  &.hidden {
    display: none;
  }

  position: fixed;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 0 auto;
  width: 100%;
  height: 50px;
  z-index: 20;
  top: 0;
  left: 0;
  right: 0;

  padding: 0 0.5rem;
  background-color: #000;
  border-bottom: 3px solid #ffc300;
  overflow-x: hidden;

  div {
    h1 {
      width: 100%;
      height: 100%;
      letter-spacing: 0.3rem;
      color: #fff;
      text-align: left;
    }
  }

  div {
    height: 100%;
    ul {
      position: relative;
      height: 100%;
      overflow: hidden;
      li {
        display: inline-block;
        padding: 0 0.75rem;
        height: 100%;
      }
      div {
        position: absolute;
        top: 0;
        height: 100%;
        z-index: -1;
        opacity: 1;
      }
    }
  }

  &.sticky-appear,
  &.sticky-enter {
    transform: translateY(-40px);
  }

  &.sticky-appear-active,
  &.sticky-enter-active {
    transform: translateY(0);
    transition: all 1400ms ease-in;
  }

  &.sticky-appear-done,
  &.sticky-enter-done {
    transform: translateY(0);
  }

  &.sticky-exit {
    transform: translateY(0);
  }

  &.sticky-exit-done {
    transform: translateY(-40px);
    transition: all 1400ms ease-out;
  }

  ${mq("small", "min")} {
    flex-direction: column;
    align-items: stretch;
    height: 90px;
    padding: 0;
    div {
      h1 {
        text-align: center;
      }
    }
    div {
      ul {
        display: flex;
        li {
          flex-grow: 1;
          padding: 0;
        }
      }
    }
  }

  ${mq("phoneSide", "min")} {
    flex-direction: row;
    div:nth-of-type(1) {
      width: 30%;
      padding: 1.5rem 0 1.5rem 0.5rem;
      h1 {
        text-align: left;
      }
    }
    div:nth-of-type(2) {
      width: 70%;
      ul {
        justify-content: flex-end;
        li {
          flex-grow: unset;
          width: 78px;
        }
      }
    }
  }

  ${mq("desktop", "min")} {
    flex-direction: row;
    div:nth-of-type(1) {
      padding: 0;
      h1 {
        text-align: left;
        padding: 1rem;
        font-size: 2rem;
      }
    }
    div:nth-of-type(2) {
      width: 70%;
      ul {
        li {
          width: 125px;
        }
      }
    }
  }
`

export const Slider = styled("div")`
  position: absolute;
  top: 0;
  height: 100%;
  z-index: -1;
  transition: all 400ms ease-in-out;
  background-color: #ffc300;
`

export const NavButton = styled("button")`
  display: inline;
  position: relative;
  font-size: 1rem;
  font-weight: 700;
  width: 100%;
  height: 100%;
  transition: all 400ms ease-in-out;

  ${mq("small", "min")} {
    font-size: 0.6rem;
  }
  ${mq("phone", "min")} {
    font-size: 0.6rem;
  }
  ${mq("phoneSide", "min")} {
    font-size: 0.7rem;
  }
  ${mq("tablet", "min")} {
    font-size: 0.8rem;
  }
  ${mq("desktop", "min")} {
    font-size: 1rem;
  }
`

// ul {
//   display: flex;
//   align-items: center;
//   li {
//     padding: 0 0.75rem;
//   }
// }

// &::after {
//   content: "";
//   display: block;
//   position: absolute;
//   left: 0;
//   bottom: 0;
//   width: 100%;
//   border-bottom: solid #ffc300 2px;
//   transform: scaleX(0);
//   transition: transform 300ms ease-in-out;
// }

// &:hover::after {
//   transform: scaleX(1);
// }
