import styled from "@emotion/styled"
import BackgroundImage from "gatsby-background-image"
import mq from "../utils/breakpoints"

export const ParallaxSection = styled("section")`
  ${mq("small", "min")} {
    display: none;
  }
  ${mq("desktop", "min")} {
    display: block;
  }
`

export const ImageBackground = styled(BackgroundImage)`
  ${mq("small", "min")} {
    display: none;
  }
  ${mq("desktop", "min")} {
    display: block;
  }
`
