import styled from "@emotion/styled"
import mq from "../utils/breakpoints"

export const WorkContainer = styled("div")`
  display: grid;
  width: 100%;
  height: 100%;
  grid-template-columns: repeat(12, 1fr);
  grid-template-rows: repeat(12, 1fr);
  grid-column-gap: 1rem;
  background-color: #333;

  ${mq("phoneSide", "min")} {
    grid-row-gap: 0.5rem;
  }
  ${mq("tabletLg", "min")} {
    grid-column-gap: 0.5rem;
  }
`

export const MainImageContainer = styled("div")`
  position: relative;

  width: 100%;
  height: 100%;
  z-index: 2;
  overflow: visible;

  ${mq("small", "min")} {
    grid-column-start: 1;
    grid-column-end: 13;
    grid-row-start: 3;
    grid-row-end: 9;
  }

  ${mq("phoneSide", "min")} {
    grid-column-start: 2;
    grid-column-end: 12;
    grid-row-start: 3;
    grid-row-end: 9;
  }

  ${mq("desktop", "min")} {
    grid-column-start: 2;
    grid-column-end: 9;
    grid-row-start: 3;
    grid-row-end: 12;
  }
`

export const MainImageButton = styled("button")`
  position: absolute;
  margin-top: auto;
  margin-bottom: auto;
  top: 0;
  bottom: 0;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  background-color: transparent;
  z-index: 2;
  transition: all 600ms ease-in;
  color: rgba(255, 255, 255, 0.7);

  div {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    i {
      font-size: 2rem;
    }
  }

  &:hover {
    color: #fff;
  }
`

export const ExpandImage = styled("button")`
  position: absolute;
  width: 50px;
  height: 50px;
  top: 0;
  right: 0;
  margin-top: 0.5rem;
  margin-right: 0.5rem;
  background-color: transparent;
  color: rgba(255, 255, 255, 0.7);
  transition: all 600ms ease-in;
  border-radius 50%;

  i {
    font-size: 1rem;
  }
`

export const SecondaryImageContainer = styled("div")`
  display: grid;
  position: relative;

  ${mq("small", "min")} {
    grid-column-start: 1;
    grid-column-end: 13;
    grid-row-start: 9;
    grid-row-end: 12;
    grid-template-columns: repeat(3, 1fr);
    grid-template-rows: none;
  }

  ${mq("phoneSide", "min")} {
    grid-column-start: 2;
    grid-column-end: 12;
    grid-template-columns: repeat(3, 1fr);
    grid-template-rows: none;
  }
  ${mq("tabletLg", "min")} {
    grid-column-start: 2;
    grid-column-end: 12;
    grid-row-start: 9;
    grid-row-end: 12;
    grid-template-columns: repeat(3, 1fr);
    grid-template-rows: none;
    grid-column-gap: 0.2rem;
  }

  ${mq("desktop", "min")} {
    grid-column-start: 9;
    grid-column-end: 12;
    grid-row-start: 3;
    grid-row-end: 12;
    grid-column-gap: 0;
    grid-row-gap: 0.3rem;
    grid-template-columns: none;
    grid-template-rows: repeat(3, 1fr);
  }
`

export const ImageContainer = styled("div")`
  position: relative;
  overflow: hidden;
`

export const ImageWrapper = styled("div")`
  position: relative;
  overflow: visible;
  transition: all 600ms ease-in-out;

  &.compress {
    width: 100%;
    height: 100%;
  }

  ${mq("small", "min")} {
    overflow: hidden;
    &.expand {
      width: 100%;
      height: 60vh;
    }
  }
  ${mq("tabletLg", "min")} {
    &.expand {
      width: 90vw;
      height: 80vh;
    }
  }
`

export const SecondaryButton = styled("button")`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: transparent;
`

export const Horizontal = styled("div")`
  width: 50px;
  height: 3px;
  background-color: #fff;
  margin-bottom: 0.3rem;
  opacity: 1;
`

export const Box = styled("div")`
  width: 15px;
  height: 15px;
  margin: 0.5rem;
  border: 2px solid #fff;
  transform: rotate(45deg);
`
