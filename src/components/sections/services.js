import React, { forwardRef } from "react"
import { css } from "@emotion/core"
// import { CSSTransition } from "react-transition-group"

import {
  ServicesContainer,
  MainHeading,
  BarberBadge,
  Divider,
  Circle,
  HorizontalLine,
  BoxContainer,
  Box1,
  Box2,
  Box3,
  Box4,
  SecondaryHeaderContainer,
  SecondaryHeader,
  SecondaryHorizontal,
  SecondarySquare,
  Description,
  HairCutSvg,
  RazorSvg,
  ShapeUpSvg,
  MassageSvg,
  StyleHairSvg,
  SecondarySvgContainer,
} from "../styled/services/servicesStyles"

const Services = (props, ref) => {
  return (
    <section
      ref={ref}
      css={css`
        width: 100%;
        background-color: #eee;
        border-top: 4px solid #ffc300;
      `}
    >
      <ServicesContainer>
        <MainHeading>Full Services</MainHeading>
        <Divider
          css={css`
            grid-column-start: 2;
            grid-column-end: 12;
          `}
        >
          <Circle
            css={css`
              width: 10px;
              height: 10px;
              margin: 0 1rem;
            `}
          />
          <BoxContainer>
            <div
              css={css`
                display: flex;
              `}
            >
              <Box1 />
              <Box2 />
            </div>
            <div
              css={css`
                display: flex;
              `}
            >
              <Box3 />
              <Box4 />
            </div>
          </BoxContainer>
          <Circle
            css={css`
              width: 10px;
              height: 10px;
              margin: 0 1rem;
            `}
          />
        </Divider>
        <Divider
          css={css`
            grid-column-start: 2;
            grid-column-end: 12;
          `}
        >
          <HorizontalLine />
          <Circle>
            <BarberBadge />
          </Circle>
          <HorizontalLine />
        </Divider>
        <Divider
          css={css`
            grid-column-start: 2;
            grid-column-end: 12;
          `}
        >
          <Circle
            css={css`
              width: 10px;
              height: 10px;
              margin: 0 1rem;
            `}
          />
          <BoxContainer>
            <div
              css={css`
                display: flex;
              `}
            >
              <Box1 />
              <Box2 />
            </div>
            <div
              css={css`
                display: flex;
              `}
            >
              <Box3 />
              <Box4 />
            </div>
          </BoxContainer>
          <Circle
            css={css`
              width: 10px;
              height: 10px;
              margin: 0 1rem;
            `}
          />
        </Divider>
        <SecondaryHeaderContainer className={"header-one"}>
          <SecondaryHeader>Haircuts &amp; HairStyles</SecondaryHeader>
          <Divider>
            <SecondaryHorizontal />
            <SecondarySvgContainer>
              <HairCutSvg />
            </SecondarySvgContainer>
            <SecondaryHorizontal />
          </Divider>
          <Description>
            Quality haircuts and hairstyles for your needs
          </Description>
          <Divider>
            <SecondaryHorizontal
              css={css`
                width: 50px;
              `}
            />
            <SecondarySquare />
            <SecondaryHorizontal
              css={css`
                width: 50px;
              `}
            />
          </Divider>
        </SecondaryHeaderContainer>
        <SecondaryHeaderContainer className={"header-two"}>
          <SecondaryHeader>Shaves &amp; Facial Trims</SecondaryHeader>
          <Divider>
            <SecondaryHorizontal />
            <SecondarySvgContainer>
              <RazorSvg />
            </SecondarySvgContainer>
            <SecondaryHorizontal />
          </Divider>
          <Description>
            A clean shave or a nice trim giving you a smooth look
          </Description>
          <Divider>
            <SecondaryHorizontal
              css={css`
                width: 50px;
              `}
            />
            <SecondarySquare />
            <SecondaryHorizontal
              css={css`
                width: 50px;
              `}
            />
          </Divider>
        </SecondaryHeaderContainer>
        <SecondaryHeaderContainer className={"header-three"}>
          <SecondaryHeader>Shape Ups &amp; Fades</SecondaryHeader>
          <Divider>
            <SecondaryHorizontal />
            <SecondarySvgContainer>
              <ShapeUpSvg />
            </SecondarySvgContainer>
            <SecondaryHorizontal />
          </Divider>
          <Description>
            Giving you precision and definition for your hair
          </Description>
          <Divider>
            <SecondaryHorizontal
              css={css`
                width: 50px;
              `}
            />
            <SecondarySquare />
            <SecondaryHorizontal
              css={css`
                width: 50px;
              `}
            />
          </Divider>
        </SecondaryHeaderContainer>
        <SecondaryHeaderContainer className={"header-four"}>
          <SecondaryHeader>Mens Facial Massage</SecondaryHeader>
          <Divider>
            <SecondaryHorizontal />
            <SecondarySvgContainer>
              <MassageSvg />
            </SecondarySvgContainer>
            <SecondaryHorizontal />
          </Divider>
          <Description>
            Soften the tension in your face and have a relaxing massage
          </Description>
          <Divider>
            <SecondaryHorizontal
              css={css`
                width: 50px;
              `}
            />
            <SecondarySquare />
            <SecondaryHorizontal
              css={css`
                width: 50px;
              `}
            />
          </Divider>
        </SecondaryHeaderContainer>
        <SecondaryHeaderContainer className={"header-five"}>
          <SecondaryHeader>Home Appoinments</SecondaryHeader>
          <Divider>
            <SecondaryHorizontal />
            <SecondarySvgContainer
              css={css`
                font-size: 1rem;
                color: #000;
              `}
            >
              <i className="fas fa-home"></i>
            </SecondarySvgContainer>
            <SecondaryHorizontal />
          </Divider>
          <Description>
            If you're too busy, we can always come to you
          </Description>
          <Divider>
            <SecondaryHorizontal
              css={css`
                width: 50px;
              `}
            />
            <SecondarySquare />
            <SecondaryHorizontal
              css={css`
                width: 50px;
              `}
            />
          </Divider>
        </SecondaryHeaderContainer>
        <SecondaryHeaderContainer className={"header-six"}>
          <SecondaryHeader>Hair Designs</SecondaryHeader>
          <Divider>
            <SecondaryHorizontal />
            <SecondarySvgContainer>
              <StyleHairSvg />
            </SecondarySvgContainer>
            <SecondaryHorizontal />
          </Divider>
          <Description>
            Do you have something in mind? Let us help you with the look you
            want
          </Description>
          <Divider>
            <SecondaryHorizontal
              css={css`
                width: 50px;
              `}
            />
            <SecondarySquare />
            <SecondaryHorizontal
              css={css`
                width: 50px;
              `}
            />
          </Divider>
        </SecondaryHeaderContainer>
      </ServicesContainer>
    </section>
  )
}

const forwardServices = forwardRef(Services)

export default forwardServices
