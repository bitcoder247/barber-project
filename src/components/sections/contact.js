import React, { forwardRef, useState } from "react"
import { css } from "@emotion/core"

import {
  ContactsContainer,
  ContactDescription,
  ContactInfoContainer,
  ContactForm,
  ContactHeader,
  ContactPersonal,
  ContactButton,
  VerticalLine,
} from "../styled/contact/contactstyles"
import { navigate } from "gatsby-link"

const Contact = (props, ref) => {
  const [state, setState] = useState({
    name: "",
    email: "",
  })

  const encode = data => {
    return Object.keys(data)
      .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
      .join("&")
  }

  const handleChange = e => {
    const { name, value } = e.target
    setState({
      ...state,
      [name]: value,
    })
  }

  const handleSubmit = e => {
    e.preventDefault()
    const form = e.target
    fetch("/", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: encode({ "form-name": form.getAttribute("name"), ...state }),
    })
      .then(() => {
        setState({
          ...state,
          name: "",
          email: "",
        })
      })
      .catch(err => alert(err))
  }

  return (
    <section
      ref={ref}
      css={css`
        width: 100%;
        background-color: #000;

        @media screen and (min-width: 320px) {
          height: 100%;
        }
        @media screen and (min-width: 1200px) {
          height: 100vh;
        }
      `}
    >
      <ContactsContainer>
        <ContactInfoContainer>
          <ContactHeader>Want To Contact Us?</ContactHeader>
          <ContactDescription>
            You can contact us at the following times for an appointment or for
            more information:
          </ContactDescription>
          <div
            css={css`
              @media screen and (min-width: 320px) {
                display: block;
                p {
                  padding: 0.5rem;
                  margin: 0.5rem;
                }
              }
              @media screen and (min-width: 1200px) {
                display: flex;
                p {
                  font-size: 1.2rem;
                  padding: 1rem 1.5rem;
                  margin: 0.5rem;
                }
              }
            `}
          >
            <p>Mon-Thurs: 9AM-5PM</p>
            <p>Sat-Sun: 11AM-3PM</p>
          </div>
          <ContactPersonal>
            <i className="fas fa-envelope" />
            <p>someemail@email.com</p>
          </ContactPersonal>
          <ContactPersonal>
            <i className="fas fa-home" />
            <p>Home Appointments available</p>
          </ContactPersonal>
          <ContactPersonal>
            <i className="fas fa-walking" />
            <p>Walk-Ins Available</p>
          </ContactPersonal>
          <VerticalLine />
        </ContactInfoContainer>
        <ContactForm>
          <form
            name={"contact"}
            method="post"
            action="/"
            data-netlify="true"
            data-netlify-honeypot="bot-field"
            onSubmit={e => handleSubmit(e)}
          >
            <input type="hidden" name="form-name" value="contact" />
            <p hidden>
              <label>
                Don't fill this out:
                <input name="bot-field" onChange={handleChange} />
              </label>
            </p>
            <div
              css={css`
                margin-bottom: 1.5rem;
              `}
            >
              <label>Name:</label>
              <input
                type="text"
                onChange={e => handleChange(e)}
                value={state.name}
                name="name"
                placeholder="Name"
              />
            </div>
            <div
              css={css`
                margin-bottom: 1.5rem;
              `}
            >
              <label>Email:</label>
              <input
                type="email"
                onChange={e => handleChange(e)}
                name="email"
                value={state.email}
                placeholder="Email"
              />
            </div>
            <ContactButton>Submit</ContactButton>
          </form>
        </ContactForm>
      </ContactsContainer>
    </section>
  )
}

const forwardContact = forwardRef(Contact)

export default forwardContact
