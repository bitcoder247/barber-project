import React, { forwardRef } from "react"
import { graphql, useStaticQuery } from "gatsby"
import { css } from "@emotion/core"
import Img from "gatsby-image"
import { CSSTransition } from "react-transition-group"

import {
  AboutSection,
  AboutHeader,
  ImageContainer,
  ImageWrapper,
  AboutTextContainer,
  AboutDivider,
  AboutLinks,
  SvgCrown,
  Horizontal,
  Circle,
  AboutDescriptors,
  HorizontalLine,
  Box,
  BackgroundYellow,
  BackgroundPink,
  HeartStyles,
  VerticalDivider,
} from "../styled/about/aboutStyles"

const About = (props, ref) => {
  const { image } = useStaticQuery(graphql`
    query {
      image: file(relativePath: { eq: "about/barber-photo-grayscale1.jpg" }) {
        sharp: childImageSharp {
          fluid(maxWidth: 750) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  `)

  return (
    <CSSTransition
      in={props.showAbout}
      timeout={600}
      appear={true}
      classNames={"about"}
    >
      <AboutSection ref={ref}>
        <BackgroundYellow className={"box-1"} />
        <BackgroundPink className={"box-1"} />
        <BackgroundYellow className={"box-2"} />
        <BackgroundPink className={"box-2"} />
        <AboutTextContainer>
          <AboutHeader>About Us</AboutHeader>
          <AboutDivider>
            <SvgCrown />
            <CSSTransition
              in={props.showAbout}
              appear={props.showAbout}
              classNames={"bar"}
              timeout={1200}
            >
              <Horizontal />
            </CSSTransition>
            <CSSTransition
              in={props.showAbout}
              appear={props.showAbout}
              classNames={"bar"}
              timeout={1200}
            >
              <Horizontal />
            </CSSTransition>
            <div
              css={css`
                display: flex;
              `}
            >
              <Circle />
              <Circle />
            </div>
          </AboutDivider>
          <p
            css={css`
              padding: 0.2rem 0.75rem;
              font-size: 1rem;
              font-weight: 700;
              text-align: center;
            `}
          >
            King Cuts is a barber business where we provide exceptional haircuts
            &amp; shaves for men. We offer you convience that you have access to
            a talented and dedicated staff for your male grooming needs. Anyone
            who sits on our chairs will be serviced professionally and feel like
            royalty.
          </p>
          <AboutDescriptors>
            <h3>Our Philosophy</h3>
            <div
              css={css`
                display: flex;
                justify-content: center;
                align-items: center;
                padding: 0.3rem;
              `}
            >
              <CSSTransition
                in={props.showAbout}
                appear={props.showAbout}
                timeout={1200}
                classNames={"line"}
              >
                <HorizontalLine />
              </CSSTransition>
              <Box />
              <CSSTransition
                in={props.showAbout}
                appear={props.showAbout}
                timeout={1200}
                classNames={"line"}
              >
                <HorizontalLine />
              </CSSTransition>
            </div>
            <p>
              Our philosophy is that every man should feel like a king in their
              own way. When someone gets a haircut it gives them a boost to
              their confidence and the perception is shared with themselves and
              others around them.
            </p>
            <h3>Our Mission</h3>
            <div
              css={css`
                display: flex;
                justify-content: center;
                align-items: center;
                padding: 0.3rem;
              `}
            >
              <CSSTransition
                in={props.showAbout}
                appear={props.showAbout}
                timeout={1200}
                classNames={"line"}
              >
                <HorizontalLine />
              </CSSTransition>
              <Box />
              <CSSTransition
                in={props.showAbout}
                appear={props.showAbout}
                timeout={1200}
                classNames={"line"}
              >
                <HorizontalLine />
              </CSSTransition>
            </div>
            <p>
              To provide our clients a well groomed haircut for whatever they
              need it for. We wish to give service to our clients, and send them
              off with professional work so that we hope satisfies our clients
              and give us contentment in our work.
            </p>
          </AboutDescriptors>
          <AboutDivider>
            <div
              css={css`
                display: flex;
              `}
            >
              <Circle />
              <Circle />
            </div>
            <CSSTransition
              in={props.showAbout}
              appear={true}
              classNames={"bar"}
              timeout={1200}
            >
              <Horizontal />
            </CSSTransition>
            <CSSTransition
              in={props.showAbout}
              appear={true}
              classNames={"bar"}
              timeout={1200}
            >
              <Horizontal />
            </CSSTransition>
          </AboutDivider>

          <div
            css={css`
              padding: 0.2rem;
              margin-top: 0.2rem;

              p {
                font-weight: 700;
                font-size: 1rem;
              }
              @media screen and (min-width: 550px) {
                text-align: center;
              }
            `}
          >
            <p>
              Check us out on social media, and if you can show some love!
              <HeartStyles>
                <i className="fas fa-heart" />
              </HeartStyles>
            </p>
            <div
              css={css`
                display: flex;
                justify-content: center;
                align-items: center;
                padding: 0.3rem 0.5rem;
              `}
            >
              <AboutLinks href="https://www.instagram.com">
                <i className="fab fa-instagram" />
              </AboutLinks>
              <AboutLinks href="https://www.facebook.com">
                <i className="fab fa-facebook"></i>
              </AboutLinks>
              <AboutLinks href="https://www.twitter.com">
                <i className="fab fa-twitter"></i>
              </AboutLinks>
              <AboutLinks href="https://www.snapchat.com">
                <i className="fab fa-snapchat"></i>
              </AboutLinks>
            </div>
          </div>
        </AboutTextContainer>
        <ImageContainer>
          <ImageWrapper>
            <Img
              fluid={image.sharp.fluid}
              style={{
                width: "100%",
                height: "100%",
                border: "5px solid #ffc300",
              }}
            />
          </ImageWrapper>
          <VerticalDivider />
        </ImageContainer>
      </AboutSection>
    </CSSTransition>
  )
}

const forwardAbout = forwardRef(About)
export default forwardAbout
