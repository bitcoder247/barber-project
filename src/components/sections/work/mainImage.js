import React from "react"
// import uuidv4 from "uuid/v4"
import Img from "gatsby-image"

import {
  ImageWrapper,
  MainImageButton,
  ExpandImage,
} from "../../styled/work/workStyles"

import useWorkImages from "../../../hooks/useWorkImages"

const MainImage = props => {
  const dataArr = useWorkImages()

  return (
    <>
      <ImageWrapper
        className={`${props.class !== "expand" ? "expand" : "compress"}`}
      >
        <Img
          fluid={dataArr[props.count][props.mainImage].image.fluid}
          style={{
            position: "absolute",
            top: "0",
            left: "0",
            width: "100%",
            height: "100%",
          }}
        />
        <MainImageButton
          style={{
            marginLeft: "0.3rem",
          }}
          onClick={props.handleNegImageSets}
        >
          <div>
            <i className="fas fa-caret-left" />
          </div>
        </MainImageButton>
        <MainImageButton
          style={{
            marginRight: "0.3rem",
            marginLeft: "auto",
            right: "0",
          }}
          onClick={props.handlePosImageSets}
        >
          <div>
            <i className="fas fa-caret-right" />
          </div>
        </MainImageButton>
        <ExpandImage onClick={props.handleExpandImage}>
          <i
            className={`fas fa-${
              props.class === "expand" ? "expand" : "compress"
            }`}
          />
        </ExpandImage>
      </ImageWrapper>
    </>
  )
}

export default MainImage
