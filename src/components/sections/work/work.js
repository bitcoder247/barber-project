import React, { useState, forwardRef } from 'react';
import { css } from '@emotion/core';

import MainImage from './mainImage';
import SecondaryImages from './secondaryImages';

import {
  WorkContainer,
  MainImageContainer,
  SecondaryImageContainer
} from '../../styled/work/workStyles';

const Work = (props, ref) => {
  const [state, setState] = useState({
    count: 0,
    mainImage: 0,
    class: 'expand'
  });

  const handleSwitchMainImage = index => {
    setState({
      ...state,
      mainImage: index,
      class: 'expand'
    });
  };
  const handlePosImageSets = () => {
    if (state.count === 2) {
      setState({
        ...state,
        count: 0,
        mainImage: 0,
        class: 'expand'
      });
    } else {
      setState({
        ...state,
        count: state.count + 1,
        mainImage: 0,
        class: 'expand'
      });
    }
  };
  const handleNegImageSets = () => {
    if (state.count === 0) {
      setState({
        ...state,
        count: 2,
        mainImage: 0,
        class: 'expand'
      });
    } else {
      setState({
        ...state,
        count: state.count - 1,
        mainImage: 0,
        class: 'expand'
      });
    }
  };

  const handleExpandImage = () => {
    if (state.class === 'expand') {
      setState({
        ...state,
        class: 'compress'
      });
    } else {
      setState({
        ...state,
        class: 'expand'
      });
    }
  };

  return (
    <section
      ref={ref}
      css={css`
        width: 100%;
        height: 70vh;
        background-color: #333;
        overflow: hidden;
        border-bottom: 4px solid #ffc300;

        @media screen and (min-width: 320px) {
          height: 100vh;
        }
        @media screen and (min-width: 992px) {
          height: 100vh;
        }
      `}
    >
      <WorkContainer>
        <MainImageContainer>
          <MainImage
            handlePosImageSets={handlePosImageSets}
            handleNegImageSets={handleNegImageSets}
            handleExpandImage={handleExpandImage}
            count={state.count}
            mainImage={state.mainImage}
            class={state.class}
          />
        </MainImageContainer>
        <SecondaryImageContainer>
          <SecondaryImages
            handleSwitchMainImage={handleSwitchMainImage}
            count={state.count}
            mainImage={state.mainImage}
          />
        </SecondaryImageContainer>
      </WorkContainer>
    </section>
  );
};

const forwardWork = forwardRef(Work);

export default forwardWork;
