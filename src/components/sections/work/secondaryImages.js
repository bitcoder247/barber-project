import React from "react"
import Img from "gatsby-image"
import uuidv4 from "uuid/v4"

import useWorkImages from "../../../hooks/useWorkImages"

import { ImageContainer, SecondaryButton } from "../../styled/work/workStyles"

const SecondaryImages = props => {
  const dataArr = useWorkImages()
  return (
    <>
      {dataArr[props.count].map((elem, index, arr) => {
        if (props.mainImage === index) return null
        return (
          <ImageContainer key={uuidv4()}>
            <Img
              fluid={elem.image.fluid}
              style={{
                position: "absolute",
                top: "0",
                left: "0",
                width: "100%",
                height: "100%",
              }}
            />
            <SecondaryButton
              onClick={() => props.handleSwitchMainImage(index)}
            />
          </ImageContainer>
        )
      })}
    </>
  )
}

export default SecondaryImages
