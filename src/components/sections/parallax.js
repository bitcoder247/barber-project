import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import { css } from "@emotion/core"

import {
  ParallaxSection,
  ImageBackground,
} from "../styled/parallax/parallaxStyles"

const Parallax = () => {
  const { image } = useStaticQuery(graphql`
    query {
      image: file(
        relativePath: { eq: "parallax/simon-infanger-unsplash-contrast.jpg" }
      ) {
        sharp: childImageSharp {
          fluid(maxWidth: 1600) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  `)

  return (
    <ParallaxSection
      css={css`
        width: 100vw;
        height: 100vh;
      `}
    >
      <div
        css={css`
          width: 100%;
          height: 100%;

          @media screen and (min-width: 320px) {
            display: none;
          }

          @media screen and (min-width: 1200px) {
            display: block;
          }
        `}
      >
        <ImageBackground
          fluid={image.sharp.fluid}
          style={{
            height: "100%",
            backgroundAttachment: "fixed",
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}
        />
      </div>
    </ParallaxSection>
  )
}

export default Parallax
