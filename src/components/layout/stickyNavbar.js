import React, {
  useState,
  useContext,
  useRef,
  useLayoutEffect,
  useCallback,
} from "react"
import { Navbar, NavButton, Slider } from "../styled/stickynav/stickyNav"
import AppContext from "../../context/app/AppContext"
import useScrollPosition from "../../hooks/useScrollPosition"

const StickyNavbar = props => {
  const [hideElem, setHideElem] = useState(true)
  const [active, setActive] = useState("About Me")

  const {
    showAbout,
    setShowAbout,
    setShowService,
    setShowContact,
    aboutScroll,
    workScroll,
    contactScroll,
    serviceScroll,
  } = props
  const appContext = useContext(AppContext)

  const navRef = useRef(null)
  const ulRef = useRef(null)
  const listRef = useRef(null)
  const slideRef = useRef(null)
  const { tabs } = appContext

  const handleClick = string => {
    switch (string) {
      case "About Me":
        setActive("About Me")
        aboutScroll()
        break
      case "My Work":
        setActive("My Work")
        workScroll()
        break
      case "Services":
        setActive("Services")
        serviceScroll()
        break
      case "Contact Me":
        setActive("Contact Me")
        contactScroll()
        break
      default:
        break
    }
  }

  const effect = useCallback(
    ({ windowPos }) => {
      let hide
      if (windowPos.y > navRef.current.nextSibling.offsetHeight) {
        hide = false
      } else if (windowPos.y < navRef.current.nextSibling.offsetHeight) {
        hide = true
      }

      if (hide !== hideElem) {
        setHideElem(hide)
        if (showAbout !== true) {
          setShowAbout(true)
        }
      }

      if (
        windowPos.y > navRef.current.nextSibling.nextSibling.offsetTop &&
        windowPos.y <
          navRef.current.nextSibling.nextSibling.nextSibling.offsetTop - 87
      ) {
        if (active !== "About Me") {
          setActive("About Me")
        }
      }
      if (
        windowPos.y >
          navRef.current.nextSibling.nextSibling.nextSibling.offsetTop &&
        windowPos.y <
          navRef.current.nextSibling.nextSibling.nextSibling.nextSibling
            .nextSibling.offsetTop -
            87
      ) {
        if (active !== "My Work") {
          setActive("My Work")
        }
      }
      if (
        windowPos.y >
          navRef.current.nextSibling.nextSibling.nextSibling.nextSibling
            .nextSibling.offsetTop &&
        windowPos.y <
          navRef.current.nextSibling.nextSibling.nextSibling.nextSibling
            .nextSibling.nextSibling.offsetTop -
            87
      ) {
        if (active !== "Services") {
          setActive("Services")
          setShowService(true)
        }
      }
      if (
        windowPos.y >
        navRef.current.nextSibling.nextSibling.nextSibling.nextSibling
          .nextSibling.nextSibling.offsetTop -
          87
      ) {
        if (active !== "Contact Me") {
          setActive("Contact Me")
          setShowContact(true)
        }
      }
    },

    //eslint-disable-next-line
    [active, hideElem, showAbout]
  )

  useScrollPosition(effect, [active, showAbout, hideElem, effect, true, 100])

  useLayoutEffect(() => {
    function handleResize() {
      if (hideElem !== true) {
        slideRef.current.style.width = `${listRef.current.offsetWidth}px`
        slideRef.current.style.left = `${listRef.current.offsetLeft}px`
      }
    }

    if (hideElem !== true) {
      slideRef.current.style.width = `${listRef.current.offsetWidth}px`
      slideRef.current.style.left = `${listRef.current.offsetLeft}px`
    }

    window.addEventListener("resize", handleResize)

    return () => {
      window.removeEventListener("resize", handleResize)
    }
  }, [active, hideElem])

  return (
    <>
      <Navbar className={hideElem ? "hidden" : "stickyNav"} ref={navRef}>
        <div>
          <h1>King Cuts</h1>
        </div>
        <div>
          <ul ref={ulRef}>
            {tabs.slice().map(tab => {
              if (active === tab.section) {
                return (
                  <li ref={listRef} key={tab.id}>
                    <NavButton
                      onClick={() => handleClick(tab.section)}
                      style={{
                        color: tab.section === active ? "#000" : "#fff",
                      }}
                    >
                      {tab.section}
                    </NavButton>
                  </li>
                )
              }
              return (
                <li key={tab.id}>
                  <NavButton
                    onClick={() => handleClick(tab.section)}
                    style={{
                      color: tab.section === active ? "#000" : "#fff",
                    }}
                  >
                    {tab.section}
                  </NavButton>
                </li>
              )
            })}
            <Slider ref={slideRef} />
          </ul>
        </div>
      </Navbar>
    </>
  )
}

export default StickyNavbar
