import React from "react"
import Helmet from "react-helmet"
import { Global, css } from "@emotion/core"
import useSiteMetaData from "../../hooks/useSiteMetaData"

const Layout = ({ children }) => {
  const { author, title, description } = useSiteMetaData()

  return (
    <>
      <Global
        styles={css`
          * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
            overflow-x: hidden;
          }

          html,
          body {
            font-family: "Open Sans Condensed", sans-serif;
            margin: 0;
            font-size: 1rem;
            line-height: 1.5;
          }

          a {
            text-decoration: none;
            cursor: pointer;
          }

          li {
            list-style: none;
          }

          button {
            border: none;
            cursor: pointer;
            padding: 0;
            background-color: transparent;
          }

          input {
            border: none;
          }

          .background-styles {
            background-position: 0% 100%;
            background-size: 55% 100%;
            background-repeat: no-repeat;
          }

          @media screen and (min-width: 768px) {
            .background-styles {
              background-position: center;
              background-size: cover;
              background-repeat: no-repeat;
            }
          }

          @media screen and (min-width: 1200px) {
            .background-styles {
              background-position: 0% 100%;
              background-size: 55% 100%;
              background-repeat: no-repeat;
            }
          }
        `}
      />
      <Helmet>
        <html lang="en" />
        <title>{title}</title>
        <meta name="author" content={author} />
        <meta name="description" content={description} />
        <link
          rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.11.2/css/all.css"
        ></link>
        <link
          rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.11.2/css/v4-shims.css"
        ></link>
      </Helmet>
      {children}
    </>
  )
}

export default Layout
