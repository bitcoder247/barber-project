import React, { forwardRef } from "react"
import { graphql, useStaticQuery } from "gatsby"
import { css } from "@emotion/core"
import Container from "../styled/utils/container"
import NavButton from "../styled/showcase/navbutton"
import { CSSTransition } from "react-transition-group"

import {
  ShowcaseContainer,
  ImageBackground,
  Darkenbackground,
} from "../styled/showcase/imageBackground"
import {
  ShowcaseHeader,
  HeaderContainer,
  BorderOne,
  BorderTwo,
  HeadingOne,
  HeadingTwo,
  HeadingThree,
  HeadingFour,
  HorizontalContainer,
  BoxContainer,
  Box1,
  Box2,
  Box3,
  Box4,
  Circle,
  RibbonContainer,
  RibbonHeadingOne,
  RibbonHeadingTwo,
  RibbonEmblem,
} from "../styled/showcase/showcaseHeader"

import {
  ShowcaseButtonContainer,
  ShowcaseButton,
} from "../styled/showcase/showcasebuttons"

import { ChevronContainer, ChevronButton } from "../styled/showcase/chevron"

import Nav from "../styled/showcase/nav"

const Showcase = (props, ref) => {
  const { image } = useStaticQuery(graphql`
    query {
      image: file(relativePath: { eq: "showcase/andi-whiskey-unsplash.jpg" }) {
        sharp: childImageSharp {
          fluid(maxWidth: 4752) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  `)

  const handleClick = string => {
    switch (string) {
      case "About Me":
        props.aboutScroll()
        break
      case "My Work":
        props.workScroll()
        break
      case "Contact Me":
        props.contactScroll()
        break
      case "Services":
        props.serviceScroll()
        break
      default:
        break
    }
  }

  return (
    <header
      css={css`
        width: 100%;
      `}
    >
      <ShowcaseContainer ref={ref}>
        <ImageBackground fluid={image.sharp.fluid} fadeIn="soft">
          <Darkenbackground />
          <Container
            css={css`
              display: flex;
              flex-direction: column;
            `}
          >
            <CSSTransition
              in={true}
              appear={true}
              classNames={"nav"}
              timeout={800}
            >
              <Nav>
                <h1>King Cuts</h1>
                <ul>
                  <li>
                    <NavButton onClick={() => handleClick("My Work")}>
                      My Work
                    </NavButton>
                  </li>
                  <li>
                    <NavButton onClick={() => handleClick("Services")}>
                      Services
                    </NavButton>
                  </li>
                  <li>
                    <NavButton onClick={() => handleClick("Contact Me")}>
                      Contact Me
                    </NavButton>
                  </li>
                  <li>
                    <NavButton onClick={() => handleClick("About Me")}>
                      About Me
                    </NavButton>
                  </li>
                </ul>
              </Nav>
            </CSSTransition>
            <CSSTransition
              in={true}
              appear={true}
              timeout={500}
              classNames={"showcase"}
            >
              <ShowcaseHeader>
                <HeaderContainer>
                  <HeaderContainer
                    css={css`
                      align-items: center;
                      flex-direction: column;
                      align-self: flex-end;
                      padding-bottom: 0.25rem;
                    `}
                  >
                    <CSSTransition
                      in={true}
                      appear={true}
                      classNames={"border"}
                      timeout={1100}
                    >
                      <BorderOne />
                    </CSSTransition>
                    <HeadingOne>Have The</HeadingOne>
                    <CSSTransition
                      in={true}
                      appear={true}
                      classNames={"border"}
                      timeout={1100}
                    >
                      <BorderTwo />
                    </CSSTransition>
                  </HeaderContainer>
                  <HeadingTwo>Elegance</HeadingTwo>
                </HeaderContainer>
                <HeaderContainer>
                  <HeadingThree>&amp; THE PRESTIGE</HeadingThree>
                </HeaderContainer>
                <HorizontalContainer>
                  <CSSTransition
                    in={true}
                    appear={true}
                    classNames={"border"}
                    timeout={1100}
                  >
                    <BorderOne
                      css={css`
                        width: 30%;
                        height: 2px;
                      `}
                    />
                  </CSSTransition>
                  <CSSTransition
                    in={true}
                    classNames={"box"}
                    timeout={1200}
                    appear={true}
                  >
                    <BoxContainer>
                      <div
                        css={css`
                          display: flex;
                        `}
                      >
                        <Box1>
                          <Circle />
                        </Box1>
                        <Box2 />
                      </div>
                      <div
                        css={css`
                          display: flex;
                        `}
                      >
                        <Box3 />
                        <Box4>
                          <Circle />
                        </Box4>
                      </div>
                    </BoxContainer>
                  </CSSTransition>
                  <CSSTransition
                    in={true}
                    appear={true}
                    classNames={"border"}
                    timeout={1100}
                  >
                    <BorderTwo
                      css={css`
                        width: 30%;
                        height: 2px;
                      `}
                    />
                  </CSSTransition>
                </HorizontalContainer>
                <HeaderContainer>
                  <HeadingFour>
                    WITH
                    <span
                      css={css`
                        font-family: "Rye";
                        padding-left: 0.3rem;
                      `}
                    >
                      GROOMING
                    </span>
                  </HeadingFour>
                </HeaderContainer>
                <HeaderContainer
                  css={css`
                    width: 50%;
                    overflow-x: visible;

                    @media screen and (min-width: 768px) {
                      width: 35%;
                    }
                    @media screen and (min-width: 1200px) {
                      width: 25%;
                    }
                  `}
                >
                  <RibbonContainer>
                    <RibbonHeadingOne>King</RibbonHeadingOne>
                    <RibbonEmblem />
                    <RibbonHeadingTwo>Cuts</RibbonHeadingTwo>
                  </RibbonContainer>
                </HeaderContainer>
              </ShowcaseHeader>
            </CSSTransition>
            <ShowcaseButtonContainer>
              <ShowcaseButton>
                <i
                  className="fas fa-briefcase"
                  css={css`
                    margin-right: 0.3rem;
                  `}
                />
                My Work
              </ShowcaseButton>
              <ShowcaseButton>
                <i
                  className="fas fa-calendar-alt"
                  css={css`
                    margin-right: 0.3rem;
                  `}
                />
                Book Now
              </ShowcaseButton>
            </ShowcaseButtonContainer>
            <ChevronContainer>
              <ChevronButton>
                Find Out More <br />
                <i
                  className="fas fa-chevron-down"
                  css={css`
                    font-size: 1.2rem;
                  `}
                />
              </ChevronButton>
            </ChevronContainer>
          </Container>
        </ImageBackground>
      </ShowcaseContainer>
    </header>
  )
}

const forwardShowcase = forwardRef(Showcase)

export default forwardShowcase
