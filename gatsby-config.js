module.exports = {
  siteMetadata: {
    title: `King Cuts | Barber Hairstylist Groomer Barber-Nomad`,
    description: `Professional haircuts, shave, shape-ups, beard care, and a modern style to for today's era`,
    author: `@digicoder`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-react-svg`,
      options: {
        rules: {
          include: `${__dirname}/content/svgs/`,
        },
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/content/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      // options: {
      //   name: `gatsby-starter-default`,
      //   short_name: `starter`,
      //   start_url: `/`,
      //   background_color: `#663399`,
      //   theme_color: `#663399`,
      //   display: `minimal-ui`,
      //   icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      // },
    },
    `gatsby-plugin-emotion`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/content/pages`,
      },
    },
    {
      resolve: `gatsby-plugin-prefetch-google-fonts`,
      options: {
        fonts: [
          {
            family: `Rye`,
            variants: ["400"],
          },
          {
            family: `Cookie`,
            variants: ["400"],
          },
          {
            family: `Open Sans Condensed`,
            variants: ["300", "700"],
          },
        ],
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
